var chatClass = require('./lib/chatClass.js');
var express = require('express');
var app = require('express')();
var http = require('http');
var https = require('https');
var server = http.Server(app);
var io = require('socket.io')(server);
var express = require('express');
var session = require('express-session');
var soap = require('soap');
var fs = require('fs');


global.fs = fs;
global.https = https;
global.http = http;


var rightNowChatUrl = null;
var rightNowChatToken = null;
var sessionMiddleware = session({secret: 'keyboard cat'});

global.chatMappings = {};
global.pollingChatUrl = false;

app.use(express.static('public'));
app.use(sessionMiddleware);
io.use(function(socket, next) {
    sessionMiddleware(socket.request, socket.request.res, next);
});


app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});
app.use(express.static('public'));

io.on('connection', function(socket){
  var session_id = socket.request.session.id;
  if(typeof global.chatMappings[socket.request.session.id] === 'undefined'){
    global.chatMappings[socket.request.session.id] = {
      chatSessionId: null,
      name: socket.request.session.name,
      email: socket.request.session.email,
      messages: []
    };
  };

  
    
    var hello_msg = {
        'name'      : 'System',
        'message'   : 'Please wait ... ' + ' (' + session_id + ')'
    };
    
    socket.join(session_id);
    
    //global.emitClientMessage(session_id, hello_msg);
    
    socket.on('chat_message', function(msg){
        console.log('->Message from: ' + session_id);
        //console.log('Mesage: ' + msg);

        //console.log(global.chatObject);
        global.chatObject.sendMessage(msg);
    });

    socket.on('system_message', function(msg){
      console.log(msg);
        console.log('->System Message from: ' + session_id);

        if(msg.type === 'getChatHistory'){
            global.chatObject.retrieveChatHistory();
        }
        
        if(msg.type === 'setUserData'){
            global.chatMappings[socket.request.session.id].firstName = msg.data.firstName;
            global.chatMappings[socket.request.session.id].lastName = msg.data.lastName;
            global.chatMappings[socket.request.session.id].email = msg.data.email;
            global.chatObject = new chatClass(session_id);
        }
    });
});

server.listen(3000, function(){
  console.log('listening on *:3000');
});

global.emitClientMessage = function(session_id, messageData) {  
    io.to(session_id).emit('message', messageData);
}