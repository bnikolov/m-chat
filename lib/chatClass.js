var parseString = require('xml2js').parseString

module.exports = function(clientSessionId){
	var url = 'https://mansion.custhelp.com/cgi-bin/mansion.cfg/services/chat_soap';

	this.clientSessionId = clientSessionId;
	this.chatSessionId = null;

	console.log(100,clientSessionId);

	if (! global.pollingChatUrl && (typeof global.rightNowChatUrl  === 'undefined' || typeof global.rightNowChatToken === 'undefined')){
		// Call GetChatURL
		global.pollingChatUrl = true;

		console.log("=== rightNowChatUrl & rightNowChatToken Are undefined... calling getChatUrl...");
		
		getChatUrl(function(result){
			console.log("=== Got getChatUrl result", result);
			global.rightNowChatUrl = result.ChatUrl;
			global.rightNowChatToken = result.ChatToken;
			global.pollingChatUrl = false;

			console.log("=== RightNow ChatURL & Token: ", global.rightNowChatUrl, global.rightNowChatToken);
		});
	}

	var sleep = function(sec){
		if(typeof global.rightNowChatUrl  === 'undefined' || typeof global.rightNowChatToken === 'undefined') {
			console.log("Sleep for " + sec + " sec");
			setTimeout(sleep, sec * 1000);
		}
		else{
                    //console.log('=========== CHAT MAPPING ===========');
                    //console.log(global.chatMappings)
                    
			if(! global.chatMappings[clientSessionId].chatSessionId){
				requestChat(function(result){
                    if(result['Fault']) {
                        // Call GetChatURL
                        global.pollingChatUrl = true;

                        getChatUrl(function(result){
                                global.rightNowChatUrl = result.ChatUrl;
                                global.rightNowChatToken = result.ChatToken;
                                global.pollingChatUrl = false;
                                
                                requestChat(function(result){ 
                                    global.chatMappings[clientSessionId].chatSessionId = result.SessionID;
                                    console.log("Chat SessionID: ", result.SessionID);
                                    //preset SessionID
                                });
                        });
                    }
					//console.log(result);
					global.chatMappings[clientSessionId].chatSessionId = result.SessionID;
					retrieveMessages();
				});
			}
			//console.log(global.chatMappings[clientSessionId]);
		}
	}

	sleep(1);
	// Init chat (call rightNow Request Chat) if not already done for this sessionId
	

	function getRequestXML(file, replaceData, cb) {
		replaceData = replaceData.concat([['username', 'api_user'], ['password', 'Lyubodil8480']]);

		var fileName = './ChatSoap/' + file + '.xml';

		global.fs.readFile(fileName, 'utf8', function (error, data) {
		  if (error) {
		    console.log(error);
		  }

		  var xml_replaced_string = data;

		  for(i = 0; i < replaceData.length; i++){
		  	xml_replaced_string = xml_replaced_string.replace('{{' + replaceData[i][0] + '}}', replaceData[i][1]);
		  }
		  //console.log(fileName);
		  cb(xml_replaced_string);
		});
	}

	function postXml(url, data, soapAction, cb){
		console.log("URL is (" + soapAction + "): " + url);
		var method = (url.indexOf('https') !== -1)?global.https:global.http;
		var urlList = url.replace('https://', '').replace('http://', '');
		urlList = urlList.split('/');
		var host = urlList.shift();
		var path = '/' + urlList.join('/');

		var post_options = {
		      host: host,
		      path: path,
		      method: 'POST',
		      headers: {
		          'Content-Type': 'text/xml;charset=UTF-8',
		          'SOAPAction': soapAction
		      }
		  };

		  // Set up the request
		  var post_req = method.request(post_options, function(res) {
		    	//res.setEncoding('utf8');
		    	var responseString = '';
		    	res.on('data', function (chunk) {
		    		responseString += chunk;
		    	});

		    	res.on('end', function() {
		    		// console.log(responseString);
                   if(soapAction == 'RequestChat'){
                        //console.log('=========== RequestChat ===========');
                        //console.log(responseString);
                    }
                    

		         	parseString(responseString, function (err, result) {
		         		if(soapAction == 'RequestChat'){
		         			// console.log(soapAction, result['S:Envelope']['S:Body'][0]);
		         			// console.log("--------");
		         		}

		         		var fn = function(obj){
				            var usefulResult = {};
				            Object.getOwnPropertyNames(obj).forEach(function(val, idx, array){
				                if(val === '$') {
				                    return;
				                }
				                                        
				                var myIndex;
				                    
				                if(val.indexOf(':') === -1) {
				                    myIndex = val;
				                } else {
				                    myIndex = val.split(':')[1];
				                }

				                if(Object.prototype.toString.call( obj[val] ) === '[object Array]'){
				                	if(obj[val].length === 1){
				                		if (typeof obj[val][0] === 'string'){
					                        usefulResult[myIndex] = obj[val][0];
					                    }
					                    else
					                        usefulResult[myIndex] = fn(obj[val][0]);
				                	}
				                	else{
				                		usefulResult[myIndex] = [];
				                		for(var i = 0; i < obj[val].length; i++){
				                			usefulResult[myIndex].push(fn(obj[val][i]));
				                		}
				                	}
				                }
				                else{
				                    usefulResult[myIndex] = fn(obj[val]);
				                }
				            });
				            return usefulResult;
				        };

				        var usefulResult = fn(result);

		         		if(soapAction == 'RequestChat'){
                                                //console.log('============= USEFULL RESULT =============')
		         			//console.log(usefulResult['Envelope']['Body']);
		         			//console.log("=====");
		         		}
                                        
                        if(usefulResult['Envelope']['Body']['Fault']) {
                            //console.log('========= Fault response =========');
                            //console.log(usefulResult['Envelope']['Body']);
                            
                            cb(usefulResult['Envelope']['Body']);
                        } else {
                            if(soapAction == "GetChatUrl"){
                                usefulResult = usefulResult['Envelope']['Body'][soapAction + 'Response'][soapAction + 'Result'];
                            }
                            else{
                                usefulResult = usefulResult['Envelope']['Body'][soapAction + 'Response'];
                            }
                            cb(usefulResult);
                        }
                        
                	});
		      });
		  });

			// console.log(data);

		  post_req.write(data);
		  post_req.end();
	}

	function callRnAction(actionName, replaceData, cb){
		getRequestXML(actionName, replaceData, function(data){
			var purl = (actionName == 'GetChatUrl')?url:global.rightNowChatUrl;
			//console.log('endpoint URL');
			//console.log(purl);

			postXml(purl, data, actionName, cb);
		})
	}

	function getChatUrl(cb){
		console.log('==================== Call getChatUrl ==================================');

		// Call RightNow endpoint and return chatUrl & ChatToken
		callRnAction('GetChatUrl', [], function(result){
			cb(result);
		});
	}

	function requestChat(cb){
		console.log('==================== Call requestChat ==================================');
		// Use rightNowChatUrl & rightNowChatToken to call rightNow's RequestChat; It will return the chat SessionID

		// Use chatMappings[clientSessionId].email & chatMappings[clientSessionId].name in CustomerInformation

		//ClientTransactionID - use 0 here

		var clientTransactionID = 0;
		var dt = new Date();
		var data = [
			['clientRequestTime', dt.toISOString()], 
			['clientTransactionId', clientTransactionID],
			['email', global.chatMappings[clientSessionId].email],
			['firstName', global.chatMappings[clientSessionId].firstName],
			['lastName', global.chatMappings[clientSessionId].lastName],
			['chatToken', global.rightNowChatToken]
		];

		callRnAction('RequestChat', data, function(result){
			console.log('================= Call RequestChat ===================================');

			//console.log(result);
			var terminateTimeout = 30;
			//console.log('Chat will be auto terminated after ' + terminateTimeout + ' seconds!');
			//setTimeout(terminateChat, terminateTimeout * 1000);
			cb(result);
		});
	}

	function retrieveMessages(){
		console.log('================= Call retrieveMessages ===================================');
		//Call rightNow's RetreiveMessages and assign new messages to messages array below
	
		var clientTransactionID = global.chatMappings[clientSessionId].messages.length + 1;
		var dt = new Date();
		var data = [
			['clientRequestTime', dt.toISOString()], 
			['clientTransactionId', clientTransactionID],
			['sessionID', global.chatMappings[clientSessionId].chatSessionId]
		];

		callRnAction('RetrieveMessages', data, function(result){
			console.log('================= Call retrieveMessages again ===================================');

			setTimeout(retrieveMessages, 1 * 1000);

			if(! result.SystemMessages) {
				return false;
			}

			var newMessages = [];

			if(result.SystemMessages.RNChatParticipantAddedMessage){
				newMessages.push({
					clientTransactionID: result.TransactionResponseData.ClientTransactionID,
					createdTime: result.SystemMessages.RNChatParticipantAddedMessage.CreatedTime,
					agentName: result.SystemMessages.RNChatParticipantAddedMessage.Name,
					message: result.SystemMessages.RNChatParticipantAddedMessage.Greeting
				});
				global.chatMappings[clientSessionId].agentName = result.SystemMessages.RNChatParticipantAddedMessage.Name;
			}
			else if(result.SystemMessages.RNChatMessagePostedMessage){
				if(Object.prototype.toString.call( result.SystemMessages.RNChatMessagePostedMessage ) === '[object Array]'){
					for(i = 0; i < result.SystemMessages.RNChatMessagePostedMessage.length; i++){
						newMessages.push({
							clientTransactionID: result.TransactionResponseData.ClientTransactionID,
							createdTime: result.SystemMessages.RNChatMessagePostedMessage[i].CreatedTime,
							agentName: global.chatMappings[clientSessionId].agentName,
							message: result.SystemMessages.RNChatMessagePostedMessage[i].Body
						});
					}
				}
				else{
					newMessages.push({
						clientTransactionID: result.TransactionResponseData.ClientTransactionID,
						createdTime: result.SystemMessages.RNChatMessagePostedMessage.CreatedTime,
						agentName: global.chatMappings[clientSessionId].agentName,
						message: result.SystemMessages.RNChatMessagePostedMessage.Body
					});
				}
			}
			else{
				return false;
			}

			emitMessages(newMessages);
			
		});	
	}

	function emitMessages(messages){
		for(i = 0; i < messages.length; i++){
			var messageData = {
				id: messages[i].clientTransactionID,
				dateTime: messages[i].createdTime, //'2015-12-17T08:39:38.433Z', // Get CreatedTime from response
				from: 'agent',
				name: messages[i].agentName, //'AgentName', //get agent name from response
				message: messages[i].message
			};

			global.chatMappings[clientSessionId].messages.push(messageData);

			global.emitClientMessage(clientSessionId, messageData);
		}
	};

	function terminateChat(){
		var clientTransactionID = global.chatMappings[clientSessionId].messages.length + 1;

		var dt = new Date();
		var data = [
			['clientRequestTime', dt.toISOString()], 
			['clientTransactionId', clientTransactionID],
			['sessionId', global.chatMappings[clientSessionId].chatSessionId]
		];

		callRnAction('TerminateChat', data, function(result){
			delete global.chatMappings[clientSessionId];
			console.log('Chat Terminated');
		});
	}

	return {
		sendMessage: function(msg){
			console.log("=================== Call sendMessage ============================");
			var checkSessionAndSend = function(seconds){
				if(typeof global.chatMappings[clientSessionId].chatSessionId  === 'undefined') {
					console.log("Sleep for " + sec + " sec");
					setTimeout(checkSessionAndSend, sec * 1000);
				}
				else{
					var clientTransactionID = global.chatMappings[clientSessionId].messages.length + 1;

					var dt = new Date();
					global.chatMappings[clientSessionId].messages.push({
						id: clientTransactionID,
						dateTime: dt.toISOString(),
						from: 'client',
						name: global.chatMappings[clientSessionId].name,
						message: msg
					});

					var data = [
						['clientRequestTime', dt.toISOString()], 
						['clientTransactionId', clientTransactionID],
						['messageBody', msg],
						['sessionId', global.chatMappings[clientSessionId].chatSessionId]
					];

					callRnAction('PostChatMessage', data, function(result){
						console.log('Successfully sent the message to RN');
					});
				}
			}
			var seconds = 1;

			checkSessionAndSend(seconds);
		},

		retrieveChatHistory: function(){
			emitMessages(global.chatMappings[clientSessionId].messages);
		}
	}
};

