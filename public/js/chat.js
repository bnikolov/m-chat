var socket = io();
$('form').submit(function(){
    var msg = $('#m');
    socket.emit('chat_message', msg.val());
    
    renderResult({
        'name'      : $('[name=name]').val(),
        'message'   : msg.val()
    });
    
    msg.val('');
    return false;
});
$('#chat-form').submit(function() {
    return false;
})

socket.on('message', function(msg){
    renderResult(msg);
});

function renderResult(msg) {
    if(msg.dateTime === undefined) {
        var d = new Date();
        msg.dateTime = d.toLocaleString();
    }
    var type = 'client';
    
    if(msg.from !== undefined) {
        type = msg.from;
    }
    
    var message = '';
    message += msg.name;
    message += ': ' + msg.message;
    
    $('#messages').append($('<li>').addClass(type).text(message));
}